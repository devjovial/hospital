<?php
    include_once 'header.php';
?>

<div class="container">

    <?php if(isset($_GET['msg'])): ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo $_GET["msg"] ?>
            </div>
        </div>
    <?php endif; ?>

    <form action="" method="post" id="schedule_form">
        <div class="row">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <h2 class="text-center">Manage doctor's schedule</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12"><br/></div>
            </div>
            <div class="col-sm-6">

                    <div class="form-group">
                        <label  for="">Select Date</label>
                        <div class="input-group date schedule_date">
                            <input type="text" id="schedule_date" name="schedule_date" class="form-control">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Select Doctor:</label>
                        <select class="form-control" name="schedule_doctor_id" id="schedule_doctor_id">

                        </select>
                    </div>


                <div class="form-group">
                        <br/>
                        <h4><b>*Booking Process:</b></h4>
                        <ol id="booking_steps">
                            <li>Select <b>DATE</b> for appointment.</li>
                            <li>Select the <b>DOCTOR</b>.</li>
                            <li>Select the <b>TIME SLOT</b> from <b>AVAILABLE</b> slots on right side.</li>
                        </ol>
                    </div>
                    <div class="form-group">
                        <br/>
                        <h4><b>* Legends:</b></h4>
                        <ul id="slots_legends">
                            <li class="bg-green">Slot available for bookings.</li>
                            <li class="bg-grey">Doctor not Available.</li>
                        </ul>
                    </div>


            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 text-right">
                        <input type="checkbox" id="schedule-checkbox" value="YesNo" style = "transform: scale(2);" disabled onclick="disableAllSlots()"> Doctor unavailable whole day<br />
                    </div>
                    <div class="col-lg-6 col-sm-6 text-right">
                        <button id="schedule-btn" type="submit" class="btn btn-default disabled">Save Schedule</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div id="schedule_slots_area">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<?php
    include_once 'footer.php';
?>


