<?php
    include_once 'header.php';
?>
Changing appointment of
<?php

        $data = getUserAppointmentById($_GET['id']);
        $schedule_data_array = mysqli_fetch_array($data['records']);

//        echo "<pre>";
//        print_r($schedule_data_array);
//        echo "</pre>";
        $choice_date = date("d/m/Y", strtotime($schedule_data_array['schedule_date'])); //formatDate($schedule_data_array['schedule_date']);
        $choice_time = convertTime($schedule_data_array['start_time']);
        echo (
            $schedule_data_array['first_name'] . " " . $schedule_data_array['last_name']
            . " with " . $schedule_data_array['name']
            . " on " . $choice_date
            . " at " . $choice_time
            . "."
        );
?>

<table width = "100%">
    <tr>

    <div class="form-group">
        <!-- removed date selection from here - it is in dump.php, if needed-->
        <td width = "50%">
        <input type="text" value="" id="new-schedule-id">
        <input type="text" value="<?=$schedule_data_array["schedule_id"]?>" id="old-schedule-id">
        <label for="name">Select Doctor:</label>
            <?php
            $response = getAllDoctors();
            $htmlOption = '<select class="form-control" name="choice_doctor_id" id="choice_doctor_id">';

            foreach($response as $record) {
                $selected = "";
                if ($record["id"] == $schedule_data_array['doctor_id']) {
                    $selected = "selected='selected'";
                }
                $htmlOption .= "<option ".$selected." value='" . $record["id"] . "'>" . $record['name'] . "</option>"; // generating option tags for each doctor record
            }

            $htmlOption .= '</select>';

            echo $htmlOption;
            ?>
        </td>
        <td>
            <div align="right">
                <button type="button" name="update-booking-btn" id="update-booking-btn" data-toggle="modal" class="btn btn-warning disabled" >Update Changes</button>
            </div>

        </td>
    </div>
    </tr>
</table>
<div class="container">
    <div class="table-responsive">
        <div class="col-sm-4">
            <div id="choice_slots_area">
                <!--grid appears here-->
                <br />
                <?php
                $dateArray = array();
                $dateStart = date('Y-m-d');
                array_push($dateArray, $dateStart);

                for ($x = 1; $x <= 6; $x++) { // one is shown by default
                    $nextday = strftime("%Y-%m-%d", strtotime("$dateStart +1 day"));
                    array_push($dateArray, $nextday);
                    $dateStart = $nextday;
                }
                // var_dump ($dateArray);
                $htmlGrid = '<table id="choice_slots_area_sub" class="table table-bordered"> <tr>';

                foreach ($dateArray as $dateItem) {
                    $htmlGrid .= '<td>';

                    $slotsResult = [];
                    $doctor_ID = $schedule_data_array['doctor_id'];
                    $booking_date = $dateItem;
                    $htmlGrid .= "<p style = 'color: black'>" . date("d/m/Y", strtotime($booking_date)). "</p>"; // $booking_date changed format

                    $sqlQuery = "SELECT ts.id as slot_id, ds.id as schedule_id,  ts.start_time, ds.status
                              FROM doctor_schedules ds INNER JOIN time_slots ts  ON ts.id = ds.slot_id
                              WHERE ds.schedule_date = '$booking_date' AND ds.doctor_id = '$doctor_ID'
                              order by slot_id
                             ";

                    $records = getRecord($sqlQuery);
                    if ($records["num"] > 0) {
                        while ($singleRecord = mysqli_fetch_assoc($records["records"])) {
                            array_push($slotsResult, $singleRecord);
                        }
                    }

                    //var_dump($slotsResult);
                    $htmlGrid .= '<table id="choice_slots_table" class="table table-bordered"> <tr>';
                    $i = 0;
                    foreach ($slotsResult as $record) {
                        if ($i % 6 == 0) {   // remainder division
                            $htmlGrid .= "</tr><tr>";
                        }
                        $strTime = convertTime($record['start_time']); // convert time to 12 hour format
                        $strColour = decideBgColour($record['status']);
                        //var_dump($record);
                        $htmlGrid .= "<td rel='" . $record['schedule_id'] . "' class='choice_slot-box bg-" . $strColour . "'>" . $strTime . "</td>";
                        $i++;
                    }
                    $htmlGrid .= '</table></td>';
                }
                $htmlGrid .= '</tr></table>';
                echo $htmlGrid;
                ?>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
<script>
    makeSlotActiveMultiTable();
</script>