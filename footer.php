       <div id="common_modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title common_modal_title">_modal_title_</h4>
                    </div>
                    <div class="modal-body common_modal_body">_modal_body_</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>

        <script src="js/script.js"> </script>
        </body>
</html>
<?php
generateDoctorSchedule();
deleteOldData();

?>