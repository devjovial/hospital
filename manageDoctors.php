<?php
include_once 'header.php';
?>

<div class="container">
    <h3 align="center">Manage doctors' records.</h3>
    <br />
    <div class="table-responsive">
        <div align="right">
            <button type="button" name="add_doctor" id="add_doctor" data-toggle="modal" data-target="#add_doctor_data_modal" class="btn btn-warning">Add doctor</button>
        </div>
        <br />
        <div id="doctor_table">
            <?php
                echo printDoctorRecordTable();
            ?>
        </div>
    </div>
</div>




<div id="view_doctor_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Doctor's Details</h4>
            </div>
            <div class="modal-body" id="doctor_detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add_doctor_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a new doctor.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="add_doctor_form">
                    <label>Enter Name</label>
                    <input type="text" name="add_doctor_name" id="add_doctor_name" class="form-control" />
                    <input type="hidden" name="fnToCall" value="addDoctor">
                    <br />
                    <label>Enter Qualifications</label>
                    <input type="text" name="add_doctor_qualification" id="add_doctor_qualification" class="form-control" />
                    <br />
                    <input type="submit" name="insert_doctor" id="insert_doctor" value="Insert Doctor" class="btn btn-success" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="edit_doctor_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit details of doctor.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="edit_doctor_form">
                    <label>ID</label>
                    <input type="text" name="edit_doctor_id" id="edit_doctor_id" class="form-control" value = "" readonly>
                    <br />
                    <label>Name</label>
                    <input type="text" name="edit_doctor_name" id="edit_doctor_name" class="form-control" value = "" readonly>
                    <br />
                    <label>Qualifications</label>
                    <input type="text" name="edit_doctor_qualification" id="edit_doctor_qualification" class="form-control" value = "" >
                    <br />
                    <input type="submit" name="update_doctor" id="update_doctor" value="Update Doctor" class="btn btn-success" />

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




<?php
include_once 'footer.php';
?>