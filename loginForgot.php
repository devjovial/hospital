<?php
    include_once 'header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4  col-centered">
                <form class="form-signin" action="" method="post">
                    <?php
                        if(isset($_GET['msg'])){
                            echo '<h3 style="color:red;">'.$_GET['msg'].'</h3>';
                            echo '<a href="loginRecover.php">forgot password? </a> ';
                        }
                    ?>
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <div class="form-group">
                        <label for="medicare1" class="sr-only">Medicare</label>
                        <table> <tr>
                                <td><input name="medicare1a" type="text" id="medicare1a" class="form-control2" pattern="[1-9]"
                                           required="" maxlength="1" data-cip-id="medicare1a" onKeyUp="check('1')"></td>
                                <td><input name="medicare2a" type="text" id="medicare2a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare2a" onKeyUp="check('2')"></td>
                                <td><input name="medicare3a" type="text" id="medicare3a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare3a" onKeyUp="check('3')"></td>
                                <td><input name="medicare4a" type="text" id="medicare4a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare4a" onKeyUp="check('4')"></td><td>----</td>
                                <td><input name="medicare1b" type="text" id="medicare1b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare1b" onKeyUp="check('5')"></td>
                                <td><input name="medicare2b" type="text" id="medicare2b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare2b" onKeyUp="check('6')"></td>
                                <td><input name="medicare3b" type="text" id="medicare3b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare3b" onKeyUp="check('7')"></td>
                                <td><input name="medicare4b" type="text" id="medicare4b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare4b" onKeyUp="check('8')"></td>
                                <td><input name="medicare5b" type="text" id="medicare5b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare5b" onKeyUp="check('9')"></td><td>----</td>
                                <td><input name="medicare1c" type="text" id="medicare1c" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare1c" onKeyUp="check('10')"></td><td>----</td>
                                <td><input name="medicare1d" type="text" id="medicare1d" class="form-control2" pattern="[1-9]"
                                           required="" maxlength="1" data-cip-id="medicare1d"></td>
                            </tr></table>
                    </div>

                    <div class="form-group">
                        <label for="password" class="sr-only">Password</label>
                        <input name="password" type="password" id="password" class="form-control"
                               placeholder="Password"
                               required="" data-cip-id="password">
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </form>
            </div>
        </div>
    </div>

<?php
    include_once 'footer.php';
?>
<?php
if(isset($_POST['medicare1a'])){
    $medicare = $_POST['medicare1a'].
        $_POST['medicare2a'].
        $_POST['medicare3a'].
        $_POST['medicare4a'].
        $_POST['medicare1a'].
        $_POST['medicare2b'].
        $_POST['medicare3b'].
        $_POST['medicare4b'].
        $_POST['medicare5b'].
        $_POST['medicare1c'].
        $_POST['medicare1d'];
    $password = $_POST['user_password'];

    $row = doLogin($medicare, $password);
    $entityType = $row['user_type'];
    $userID = $row['id'];

    $location = 'login.php?errorMsg=Sign in - Try Again.'; // initialised $location
    $_SESSION['entityType'] = $entityType;
    $_SESSION['userID'] = $userID;

    if($entityType=="manager") {
        $location = 'index.php?msg="Welcome, ' . $row['first_name'] . ' ' . $row['last_name'] . '. You are logged in as a manager"';
    }else if($entityType=="admin"){
        $location = 'index.php?msg="Welcome, '.$row['first_name']. ' '.$row['last_name'].'. You are logged in as an administrator"';
    }else if ($entityType=="user"){
        $location = 'index.php?msg="Welcome, '.$row['first_name']. ' '.$row['last_name'].'. You are logged in as a user"';
    }else {
        $location = "loginForgot.php?msg='Sign in failed - Try Again'";
    }

    header("location: $location");
}
?>
