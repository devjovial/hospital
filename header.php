<?php
    include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="css/jquery.timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css" />

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/style.css" />

    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="http://placehold.it/150x60/000?text=Hospital Appointments" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">Home</a></li>
                        <?php if(isset($_SESSION['isLogged'])): ?>
                            <li><a href="bookings.php">Bookings</a></li>
                            <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"> Logout</a></li>
                            <?php if($_SESSION['entityType'] == "admin" or $_SESSION['entityType'] == "manager"): ?>
                                    <li class="dropdown">
                                        <a href="index.php"
                                           class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                           aria-expanded="false">Admin<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <?php if($_SESSION['entityType'] == "manager"): ?>
                                                <li><a href="manageUsers.php">Manage Users</a></li>
                                                <li><a href="manageDoctors.php">Manage Doctors</a></li>
                                                <li><a href="manageTimeSlots.php">Manage Time Slots</a></li>
                                                <li role="separator" class="divider"></li>
                                            <?php endif; ?>
                                            <li><a href="manageDoctorSchedules.php">Manage Doctor Schedules</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="manageUserAppointments.php">Manage User Appointments</a></li>                                        <li role="separator" class="divider"></li>
                                        </ul>
                                    </li>

                            <?php endif; ?>
                            <li><a href="feedback.php">Feedback</a></li>
                        <?php else: ?>
                            <li><a href="login.php"><span class="glyphicon glyphicon-log-in"> Login</a></li>
                            <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <?php endif; ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>