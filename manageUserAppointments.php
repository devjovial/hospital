<?php
include_once 'header.php';
?>

<div class="container">

    <?php if(isset($_GET['msg'])): ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo $_GET["msg"] ?>
            </div>
        </div>
    <?php endif; ?>

    <form action="" method="post" id="appointments_form">
        <div class="row">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <h2 class="text-center">Manage user's appointment.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12"><br/></div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Select Date</label>
                    <div class="input-group date appointments_date">
                        <input type="text" id="appointments_date" name="appointments_date" class="form-control"><span
                                class="input-group-addon"><i
                                    class="glyphicon
                            glyphicon-calendar"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Select Doctor:</label>
                    <select class="form-control" name="appointments_doctor_id" id="appointments_doctor_id">

                    </select>
                </div>
            </div>
        </div>
    </form>

    <div class="table-responsive">
        <div id="appointments_table">

        </div>
    </div>
</div>

<div id="view_appointments_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Appointments' Details</h4>
            </div>
            <div class="modal-body" id="appointment_detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>