<?php
include_once 'header.php';
?>

<div class="container">
    <h3 align="center">Manage Time Slots.</h3>
    <br />
    <div class="table-responsive">
        <div align="right">
            <button type="button" name="add_slot" id="add_slot" data-toggle="modal" data-target="#add_slot_data_modal"
                    class="btn btn-warning">Add Slot</button>
        </div>
        <br />
        <div id="slot_table">
            <?php
                echo printTimeSlotsTable();
            ?>
        </div>
    </div>
</div>

<div id="view_slot_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Slot Details</h4>
            </div>
            <div class="modal-body" id="slot_detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add_slot_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a new slot.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="add_slot_form">
                    <input type="hidden" name="fnToCall" value="addSlot">

                    <label>Start Time</label>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" name="add_slot_start_time" id="add_slot_start_time" class="form-control input-small" />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>

                    <br />
                    <label>End Time</label>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" name="add_slot_end_time" id="add_slot_end_time" class="form-control input-small" readonly />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
<!--
                    <br />
                    <label>Slot Duration</label>
                    <input type="text" readonly name="add_slot_duration" id="add_slot_duration" class="form-control" />
-->
                    <br />
                    <input type="submit" name="insert_slot" id="insert_slot" value="Insert Slot" class="btn
                    btn-success" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="edit_slot_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit details of doctor.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="edit_slot_form">
                    <label>ID</label>
                    <input type="text" name="edit_slot_id" id="edit_slot_id" class="form-control" value = "" readonly>
                    <br />
                    <label>Start Time</label>
                    <input type="text" name="edit_slot_start_time" id="edit_slot_start_time" class="form-control" value = "" >
                    <br />
                    <label>End Time</label>
                    <input type="text" name="edit_slot_end_time" id="edit_slot_end_time" class="form-control" value = "" >
                    <br />
                    <input type="submit" name="update_slot" id="update_slot" value="Update Slot" class="btn btn-success" />

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




<?php
include_once 'footer.php';
?>