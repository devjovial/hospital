<?php
    include_once 'header.php';
?>

<div class="container">

    <?php if(isset($_GET['msg'])): ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo $_GET["msg"] ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-4 col-sm-6">
            <h1>Welcome to Our Page</h1>
        </div>
        <div class="col-md-4">
            <h1>Welcome to Our 1</h1>
        </div>
        <div class="col-md-4">
            <h1>Welcome to Our 2</h1>
        </div>
    </div>
</div>

<?php
    include_once 'footer.php';
?>