<?php
    include_once 'header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4  col-centered">
                <form class="form-signin" action="" method="post">

                    <h3 class="form-signin-heading">Password will be emailed to you.</h3>
                    <div class="form-group">
                        <label for="medicare1" class="sr-only">Medicare</label>
                        <table> <tr>
                                <td><input name="medicare1a" type="text" id="medicare1a" class="form-control2" pattern="[1-9]" autofocus=""
                                           required="" maxlength="1" data-cip-id="medicare1a" onKeyUp="check('1')"></td>
                                <td><input name="medicare2a" type="text" id="medicare2a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare2a" onKeyUp="check('2')"></td>
                                <td><input name="medicare3a" type="text" id="medicare3a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare3a" onKeyUp="check('3')"></td>
                                <td><input name="medicare4a" type="text" id="medicare4a" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare4a" onKeyUp="check('4')"></td><td>----</td>
                                <td><input name="medicare1b" type="text" id="medicare1b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare1b" onKeyUp="check('5')"></td>
                                <td><input name="medicare2b" type="text" id="medicare2b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare2b" onKeyUp="check('6')"></td>
                                <td><input name="medicare3b" type="text" id="medicare3b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare3b" onKeyUp="check('7')"></td>
                                <td><input name="medicare4b" type="text" id="medicare4b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare4b" onKeyUp="check('8')"></td>
                                <td><input name="medicare5b" type="text" id="medicare5b" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare5b" onKeyUp="check('9')"></td><td>----</td>
                                <td><input name="medicare1c" type="text" id="medicare1c" class="form-control2" pattern="[0-9]"
                                           required="" maxlength="1" data-cip-id="medicare1c" onKeyUp="check('10')"></td><td>----</td>
                                <td><input name="medicare1d" type="text" id="medicare1d" class="form-control2" pattern="[1-9]"
                                           required="" maxlength="1" data-cip-id="medicare1d"></td>
                            </tr></table>
                    </div>

                    <div class="form-group">
                        <label for="email" class="sr-only">Email address</label>
                        <input name="email" type="email" id="email" class="form-control" placeholder="registered email
                        address"
                               required="" data-cip-id="email">
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Recover Password</button>
                </form>
            </div>
        </div>
    </div>

<?php
    include_once 'footer.php';
?>
<?php
if(isset($_POST['medicare1a'])){
    $medicare = $_POST['medicare1a'].
        $_POST['medicare2a'].
        $_POST['medicare3a'].
        $_POST['medicare4a'].
        $_POST['medicare1a'].
        $_POST['medicare2b'].
        $_POST['medicare3b'].
        $_POST['medicare4b'].
        $_POST['medicare5b'].
        $_POST['medicare1c'].
        $_POST['medicare1d'];
    $email = $_POST['email'];

    $row = loginRecover($medicare, $email);

    if ($_SESSION['isUser']){
        // send email
        mail($email,"Hospital Login Password",dec_enc('decrypt',$row['password']));
        $location = 'index.php?msg="Password has been emailed to your registered email address"';
    }
    else {
        $location = "loginForgot.php?msg='Something went wrong'";
    }

    header("location: $location");
}
?>
