var schedule_id ="";
var user_id ="";
//var ajaxResult=[];
$(document).ready(function(){


    //datePickers
    $('#add_slot_start_time').timepicker({
        'scrollDefaultNow': 'true',
        'closeOnWindowScroll': 'true',
        'showDuration': true,
        'step': 15
    }).on('changeTime', function(){
        var new_end = $(this).timepicker('getTime');
        new_end.setMinutes(new_end.getMinutes()+15);
        $('#add_slot_end_time').timepicker('setTime', new_end);
        $('#add_slot_end_time').focus();
    });

    $('#add_slot_end_time').timepicker({
        'scrollDefaultNow': 'true',
        'closeOnWindowScroll': 'true',
        'showDuration': true,
        'step': 15
    }).on('selectTime', function(){

        var diffMilliSec = $('#add_slot_end_time').timepicker('getTime') - $('#add_slot_start_time').timepicker('getTime');
        diffMilliSec = diffMilliSec / 1000;
        diffMilliSec = (diffMilliSec / 3600);


        if(diffMilliSec<1){

            switch (diffMilliSec){
                case 0.25:
                    duration = "15 Minutes";
                break;

                case 0.5:
                    duration = "30 Minutes";
                break;

                case 0.75:
                    duration = "45 Minutes";
                    break;
            }
        }
        else{
            duration = diffMilliSec + " Hour";
            if(diffMilliSec>1)
                duration += "s";
        }

        //$('#add_slot_duration').val(duration);
    });

    // manageUsers page coding - START
    $('#add_user').click(function(){
        $('#insert_user').val("Insert");
        $('#add_user_form')[0].reset();
    });

    $('#add_user_form').on("submit", function(event){
        event.preventDefault(); //prevent the event, here form submission is prevented
        if($('#add_user_fname').val() == "") // "$" means jquery, read jquery selectors,  # means ID selector
        {
            alert("First Name is required");
        }
        else if($('#add_user_lname').val() == '')
        {
            alert("Last name is required");
        }
        else if($('#add_user_pass').val() == '')
        {
            alert("Password is required");
        }
        else if($('#add_user_email').val() == '')
        {
            alert("Email is required");
        }
        else if($('#add_user_phone').val() == '')
        {
            alert("Phone is required");
        }
        else
        {
            $.ajax({ // AJAX is a method of jquery so starts with $ sign, objects are passed as parameters, object is in curly braces
                url:"initFunctions.php", //AJAX sends
                method:"POST", // with method POST
                data:$('#add_user_form').serialize(), // data is also an object, example: {id:"Daler", pwd:"hello"}
                beforeSend:function(){ // server te send karan to pehlan jo marje eht kar lavo
                    $('#insert').val("Inserting");
                },
                success:function(response){ // after successful AJAX request ,

                    response = JSON.parse(response);
                    if(response.success){ // true
                        $('#add_user_form')[0].reset(); // clear fields
                        $('#add_user_data_modal').modal('hide'); // now hide the model
                        $('#user_table').html(response.data);
                        // response came from insert file (anything printed on insert.php) , response is updated with new user
                    }
                    else{

                        /*Event Binding*/
                        $('#common_modal').on('hidden.bs.modal', function () {
                            //event when the common modal for showing message is closed

                            /*Unbind Events*/
                            $('#add_user_data_modal').unbind('hidden.bs.modal');
                            $('#common_modal').unbind('hidden.bs.modal');
                            /*Unbind Events*/

                            $('#add_user_data_modal').modal('show');
                            // get the add user modal back when error modal is closed
                        });

                        $('#add_user_data_modal').on('hidden.bs.modal', function () {
                            //event when the add user modal is closed
                            showModalMessage('Error', response.data);
                            // show comman message with title Error and Body = response from server
                        });
                        /*Event Binding*/

                        $('#add_user_data_modal').modal('hide'); // now hide the model

                    }
                }
            });
        }
    });

    $(document).on('click', '.view_user_data', function(){
        //$('#dataModal').modal();
        var user_id = $(this).attr("id");
        console.log(user_id);
        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data:{user_id:user_id, fnToCall:'selectUser'},
            success:function(data){
                $('#user_detail').html(data);
                $('#view_user_data_modal').modal('show');
            }
        });
    });

    $(document).on('click', '.edit_user_data', function(){

        var id = $(this).attr("id");
        $.ajax({

            url:"initFunctions.php",
            method: "POST",
            data:{user_id:id,fnToCall:'getUserByID'},
            success:function(my_response){

                my_response = JSON.parse(my_response); // string json to object json convert
                var first_name = my_response.first_name;
                var last_name = my_response.last_name;
                var email = my_response.email;
                var phone = my_response.phone;
                var user_type = my_response.user_type;

                /*
                    javascript selector by id - document.getElementById("id_of_the_element")
                    jQuery selector by id - $("#id_of_the_element");
                    study jquery selectors
                */

                $('#edit_user_id').val(id);
                $('#edit_user_fname').val(first_name); // val() jquery method study
                $('#edit_user_lname').val(last_name);
                $('#edit_user_email').val(email);
                $('#edit_user_phone').val(phone);
                $('#edit_user_userType').html(user_type);

                $('#edit_user_data_modal').modal('show');
            }
        });
    });

    $(document).on('submit', '#edit_user_form', function(event){

        event.preventDefault(); // we do not want the page to refresh on submission of the form
        var user_form_data = $(this).serialize(); // form data only , firstname=Daler&lastname=Singh&...
        user_form_data += "&fnToCall=updateUser";  // adding fnToCall to form data

        $.ajax({
            url:"initFunctions.php",
            method: "POST",
            data: user_form_data, // another method of writing is =  {fname:$("#update_fname").val(), lname: $("update_lname").val(),
            // fnToCall:'updateUser'} example without serialize
            success:function(my_response){
                $('#edit_user_data_modal').modal('hide');
                $("#user_table").html(my_response);
            }
        });
    });

    $(document).on('click','.delete_user_data',function (event) {
        var id = $(this).attr("id");
        console.log(id);

        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            data:{userid:id,fnToCall:"deleteUser"},
            success:function (response) {
                console.log(response);
                $("#user_table").html(response);
            }
        });
    });
    // manageUsers page coding - END

    // manageDoctors page coding - START
    $('#add_doctor').click(function(){
        $('#insert_doctor').val("Insert");
        $('#add_doctor_form')[0].reset();
    });

    $('#add_doctor_form').on("submit", function(event){
        event.preventDefault(); //prevent the event, here form submission is prevented
        if($('#doctor_name').val() == "") // "$" means jquery, read jquery selectors,  # means ID selector
        {
            alert("Name is required");
        }
        else if($('#doctor_qualification').val() == '')
        {
            alert("Qualification is required");
        }
        else
        {
            $.ajax({ // AJAX is a method of jquery so starts with $ sign, objects are passed as parameters, object is in curly braces
                url:"initFunctions.php", //AJAX sends
                method:"POST", // with method POST
                data:$('#add_doctor_form').serialize(), // data is also an object, example: {id:"Daler", pwd:"hello"}
                beforeSend:function(){ // server te send karan to pehlan jo marje eht kar lavo
                    $('#insert').val("Inserting");
                },
                success:function(response){ // after successful AJAX request ,
                    $('#add_doctor_form')[0].reset(); // clear fields
                    $('#add_doctor_data_modal').modal('hide'); // now hide the model
                    $('#doctor_table').html(response); // response came from insert file (anything printed on insert.php) , response is updated with new user,
                }
            });
        }
    });

    $(document).on('click', '.view_doctor_data', function(){
        //$('#dataModal').modal();
        var doctor_id = $(this).attr("id");
        console.log(doctor_id);
        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data:{doctor_id:doctor_id, fnToCall:'selectDoctor'},
            success:function(data){
                $('#doctor_detail').html(data);
                $('#view_doctor_data_modal').modal('show');
            }
        });
    });

    $(document).on('click', '.edit_doctor_data', function(){

        var id = $(this).attr("id");
        $.ajax({

            url:"initFunctions.php",
            method: "POST",
            data:{doctor_id:id,fnToCall:'getDoctor'},
            success:function(my_response){

                my_response = JSON.parse(my_response); // string json to object json convert
                var name = my_response.name;
                var qualification = my_response.qualification;

                /*
                    javascript selector by id - document.getElementById("id_of_the_element")
                    jQuery selector by id - $("#id_of_the_element");
                    study jquery selectors
                */

                $('#edit_doctor_id').val(id);
                $('#edit_doctor_name').val(name); // val() jquery method study
                $('#edit_doctor_qualification').val(qualification);
                $('#edit_doctor_data_modal').modal('show');
            }
        });
    });

    $(document).on('submit', '#edit_doctor_form', function(event){

        event.preventDefault(); // we do not want the page to refresh on submission of the form
        var doctor_form_data = $(this).serialize(); // form data only , firstname=Daler&lastname=Singh&...
        doctor_form_data += "&fnToCall=updateDoctor";  // adding fnToCall to form data

        $.ajax({
            url:"initFunctions.php",
            method: "POST",
            data: doctor_form_data, // another method of writing is =  {fname:$("#update_fname").val(), lname: $("update_lname").val(),
            // fnToCall:'updateDoctor'} example without serialize
            success:function(my_response){
                $('#edit_doctor_data_modal').modal('hide');
                $("#doctor_table").html(my_response);
            }
        });
    });

    $(document).on('click','.delete_doctor_data',function (event) {
        var id = $(this).attr("id");
        console.log(id);

        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            data:{doctor_id:id,fnToCall:"deleteDoctor"},
            success:function (response) {
                console.log(response);
                $("#doctor_table").html(response);
            }
        });
    });

    // manageDoctors page coding - END

    // manageSlots page coding - START
    $('#add_slot').click(function(){
        $('#insert_slot').val("Insert");
        $('#add_slot_form')[0].reset();
    });

    $('#add_slot_form').on("submit", function(event){
        event.preventDefault();
        if($('#add_slot_start_time').val() == "")
        {
            alert("Start time is required");
        }
        /*
        else if($('#add_slot_end_time').val() == '')
        {
            alert("End time is required");
        }
        */
        else
        {
            $.ajax({
                url:"initFunctions.php",
                method:"POST",
                data:$('#add_slot_form').serialize(),
                beforeSend:function(){
                    $('#insert').val("Inserting");
                },
                success:function(response){
                    response = JSON.parse(response);
                    if(response.success){
                        $('#add_slot_form')[0].reset();
                        $('#add_slot_data_modal').modal('hide');
                        $('#slot_table').html(response.data);
                    }
                    else {
                        alert(response.data);
                    }
                }
            });
        }
    });

    $(document).on('click', '.view_slot_data', function(){
        //$('#dataModal').modal();
        var slot_id = $(this).attr("id");
        console.log(slot_id);
        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data:{slot_id:slot_id, fnToCall:'selectSlot'},
            success:function(data){
                $('#slot_detail').html(data);
                $('#view_slot_data_modal').modal('show');
            }
        });
    });

    $(document).on('click', '.edit_slot_data', function(){

        var id = $(this).attr("id");
        $.ajax({

            url:"initFunctions.php",
            method: "POST",
            data:{slot_id:id, fnToCall:'getSlot'},
            success:function(my_response){

                my_response = JSON.parse(my_response);
                console.log(my_response);
                var start_time = my_response.start_time;
                var end_time = my_response.end_time;

                $('#edit_slot_id').val(id);
                $('#edit_slot_start_time').val(start_time);
                $('#edit_slot_end_time').val(end_time);

                $('#edit_slot_data_modal').modal('show');
            }
        });
    });

    $(document).on('submit', '#edit_slot_form', function(event){

        event.preventDefault(); // we do not want the page to refresh on submission of the form
        var slot_form_data = $(this).serialize(); // form data only , firstname=Daler&lastname=Singh&...
        console.log(slot_form_data);
        slot_form_data += "&fnToCall=updateSlot";  // adding fnToCall to form data
        console.log(slot_form_data);
        $.ajax({
            url:"initFunctions.php",
            method: "POST",
            data: slot_form_data,

            success:function(my_response){
                $('#edit_slot_data_modal').modal('hide');
                $("#slot_table").html(my_response);
            }
        });
    });

    $(document).on('click','.delete_slot_data',function (event) {
        var id = $(this).attr("id");
        console.log(id);

        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            data:{slot_id:id,fnToCall:"deleteSlot"},
            success:function (response) {
                console.log(response);
                $("#slot_table").html(response);
            }
        });
    });
    // manageSlots page coding - END

    // manage-user-appointments page coding - START
    $(document).on('click', '.view_appt_data', function(){
        //$('#dataModal').modal();
        var user_id = $(this).attr("id");
        console.log(user_id);
        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data:{user_id:user_id, fnToCall:'selectUser'},
            success:function(response){
                console.log(response);
                $('#appointment_detail').html(response);
                $('#view_appointments_data_modal').modal('show');
            }
        });
    });

    /*delete appointment*/
    $(document).on('click','.delete_appt_data',function (event) {
        var id = $(this).attr("id");


        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            data:{user_appt_sch_id:id,fnToCall:"deleteAppointment"},
            success:function (response) {
                console.log(response);
                $("#appointment_detail").html(response);
            }
        });
    });
    // manage-user-appointments page coding - START

    //start - Book button in booking.php
    $(document).on('submit', '#booking_form', function(event){

        event.preventDefault();

        var formData = $(this).serialize();
        formData = formData+"&fnToCall=insertBookingRecord";
        console.log(formData);

        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data: formData,
            success:function(response){
                response = JSON.parse(response);
                console.log(response);
                if(response.success){
                    window.location= "./bookingSuccessful.php?msg=Booking was successful.";
                }
                else{
                    alert(response.data);
                }
            }
        });
    });
    // end - Book button in booking.php

    // start - Save button in manageDoctorSchedules.php
    $(document).on('submit', '#schedule_form', function(event){

        event.preventDefault();
        var tempBool = disableAllSlots();
        var formData = $(this).serialize();
        formData = formData+"&fnToCall=updateDoctorSchedules";
        formData = formData +"&docAwayWholeDay=";
        formData = formData + tempBool;
        console.log(formData);

        $.ajax({
            url:"initFunctions.php",
            method:"POST",
            data: formData,
            success:function(response){
                response = JSON.parse(response);
                console.log(response);
                if(response.success){
                    window.location= "/manageDoctorSchedulesSuccessful.php?msg=Schedule was changed successfully.";
                }
                else{
                    alert(response.data);
                }
            }
        });
    });
    // end - Save button in manageDoctorSchedules.php

    // My DatePickers
    $('.input-group.date.booking_date').datepicker({
                            format : "dd/mm/yyyy",
                            autoclose: true,
                            startDate: getNextDate(0), // required dd/mm/yyyy
                            endDate: getNextDate(6)
                          })
                          .on('changeDate', function(e){
                                var selectedDate = e.format('yyyy-mm-dd');
                                var useJoin = "Yes";
                                $.ajax({

                                    url:"initFunctions.php",
                                    method:"POST",
                                    data: {bookingDate:selectedDate, joinQuery:useJoin, fnToCall: "getDoctor"},
                                    success: function(res){
                                        console.log(res);
                                        res = JSON.parse(res);
                                        console.log(res);
                                        var htmlOption = "<option value=''>Select Doctor</option>";

                                        /*for(var i=0;i<res.length;i++){}*/
                                        for(x in res){
                                            var record = res[x];
                                            htmlOption += "<option value='"+record.id+"'>"+record.name+"</option>"; // generating option tags for each doctor record
                                            //<option value="doctor_id">Doctor Name</option>
                                            // console.log(x);
                                            // console.log(res[x]);
                                        }

                                        $("#booking_doctor_id").html(htmlOption);
                                        $("#booking_doctor_id").focus();
                                    }
                                });
                          });

    $('.input-group.date.schedule_date').datepicker({
                        format : "dd/mm/yyyy",
                        autoclose: true,
                        startDate: getNextDate(7),
                        endDate: getNextDate(13)
                      })
                      .on('changeDate', function(e){
                            var selectedDate = e.format('yyyy-mm-dd');
                            var useJoin = "Yes";
                            $.ajax({

                                url:"initFunctions.php",
                                method:"POST",
                                data: {bookingDate:selectedDate, joinQuery:useJoin, fnToCall: "getDoctor"},
                                success: function(res){
                                    // console.log(res);
                                    res = JSON.parse(res);
                                    //console.log(res);
                                    var htmlOption = "<option value=''>Select Doctor</option>";

                                    /*for(var i=0;i<res.length;i++){}*/
                                    for(x in res){
                                        var record = res[x];
                                        htmlOption += "<option value='"+record.id+"'>"+record.name+"</option>"; // generating option tags for each doctor record
                                        //<option value="doctor_id">Doctor Name</option>
                                        // console.log(x);
                                        // console.log(res[x]);
                                    }
                                    $("#schedule_doctor_id").html(htmlOption);
                                    $("#schedule_doctor_id").focus();
                                }
                            });
                      });

    $('.input-group.date.appointments_date').datepicker({
        format : "dd/mm/yyyy",
        autoclose: true,
        startDate: getNextDate(0), // required dd/mm/yyyy
        endDate: getNextDate(6)
    })
        .on('changeDate', function(e){
            var selectedDate = e.format('yyyy-mm-dd');
            var useJoin = "Yes";
            $.ajax({

                url:"initFunctions.php",
                method:"POST",
                data: {bookingDate:selectedDate, joinQuery:useJoin, fnToCall: "getDoctor"},
                success: function(res){
                    // console.log(res);
                    res = JSON.parse(res);
                    //console.log(res);
                    var htmlOption = "<option value=''>Select Doctor</option>";

                    /*for(var i=0;i<res.length;i++){}*/
                    for(x in res){
                        var record = res[x];
                        htmlOption += "<option value='"+record.id+"'>"+record.name+"</option>"; // generating option tags for each doctor record
                        //<option value="doctor_id">Doctor Name</option>
                        // console.log(x);
                        // console.log(res[x]);
                    }
                    $("#appointments_doctor_id").html(htmlOption);
                    $("#appointments_doctor_id").focus();
                }
            });
        });
    //$('.input-group.date').on('changeDate') other way

    /*$("#doctor_id").on('change', function(){

    });*/

    // manage-user-choices main page
    $('.bg-green').click(function () {

        $('.bg-green').removeClass('selected-box');
        $(this).addClass('selected-box');
        $('#update-btn').removeClass('disabled');

        var schedule_id = $(this).attr('rel');
        $('#schedule-id').val(schedule_id);

    });

    // manage-user-choices START


    $("#choice_doctor_id").change(function(){
        var myDate = new Date(); // get current date
        var currentDate = formatDateYmd(myDate);// change format

        var booking_doctor_id = $(this).val(); // change event is on doctor_id, this means doctor_id fields, picking its value
        var slotsArea = $("#choice_slots_area");
        var slotsSelectable ="Single";
        console.log(currentDate, booking_doctor_id, slotsArea, slotsSelectable);
        generateSlotsMultiTable(currentDate, booking_doctor_id, slotsArea, slotsSelectable);
    });

    // manage-user-choices END

    $("#booking_doctor_id").change(function(){

        var booking_date= $("#booking_date").val();
        var booking_doctor_id = $(this).val(); // change event is on doctor_id, this means doctor_id fields, picking its value
        var slotsArea = $("#booking_slots_area");
        var slotsSelectable ="Single";
        generateSlots(booking_date, booking_doctor_id, slotsArea, slotsSelectable);
    });

    $("#schedule_doctor_id").change(function(){
        document.getElementById("schedule-checkbox").disabled= false;
        var schedule_date= $("#schedule_date").val();
        var schedule_doctor_id = $(this).val(); // change event is on doctor_id, this means doctor_id fields, picking its value
        var slotsArea = $("#schedule_slots_area");
        var slotsSelectable ="Multi";
        generateSlots(schedule_date, schedule_doctor_id, slotsArea, slotsSelectable);
    });

    $("#appointments_doctor_id").change(function(){

        var appointments_doctor_id = $(this).val(); // change event is on doctor_id, this means doctor_id fields, picking its value
        var appointments_date= $("#appointments_date").val();

        console.log(appointments_date);
        console.log(appointments_doctor_id);

        // Display slot table on RHS - coded by Jovial+Bobby

        var selectedDate = $("#appointments_date").val(); // booking_date.format('yyyy-mm-dd');
        console.log(selectedDate);
        $.ajax({

            url:"initFunctions.php",
            method:"POST",
            data: {appointments_date:selectedDate, appointments_doctor_id: appointments_doctor_id, fnToCall: "printAppointmentsTable"},
            success: function(resp){
                console.log(resp);
                resp = JSON.parse(resp); // changes the data received into proper html code
                console.log(resp);
                $("#appointments_table").html(resp);
            }
        });
    });

    $("#update-booking-btn").click(function () {

        var nScheduleID = $("#new-schedule-id").val(); // get the new schdule id selected
        var oScheduleID  = $("#old-schedule-id").val(); // get the previous booking id

        var ajaxData = {nScheduleId: nScheduleID, oScheduleId: oScheduleID, fnToCall: "updateAppointment"};

        //send ajax function
        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            data: ajaxData,
            success: function (resp) {
                //redirect to page
            }
        })

    });

});

function decideBgColour(cellStatus) {
    var strBgColour = 'aqua';
    if (cellStatus === 'available'){
        strBgColour = 'green';
    } else if (cellStatus === 'booked'){
        strBgColour = 'red';
    }else if (cellStatus === 'unavailable'){
        strBgColour = 'grey';
    }
    return strBgColour;
}

function getNextDate(addDays){

    /*console.log(addDays);*/
    if(typeof addDays==="undefined"){
        addDays = 7;
    }

    var time = new Date(); // today's date object
    var days = parseInt(addDays); // string to number conversion

    time.setDate(time.getDate()+ days); // setting the date to new date in Date Object adding days
    var strdate = formatDate(time); // convert date object to string format

    /*console.log(strdate);*/
    return strdate;
}

function formatDate(date) {

    var mm = date.getMonth() + 1; // getMonth() is zero-based
    var dd = date.getDate();

    return [
            (dd>9 ? '' : '0') + dd, // dd example 10
            (mm>9 ? '' : '0') + mm, // mm example 12
            date.getFullYear()  // yyyy example 2017
        ]
        .join('/');
}

function convertTime (isoTime) {
    var hours   = parseInt(isoTime.substring(0, 2), 10),
        minutes = isoTime.substring(3, 5),
        ampm    = ' AM';

    if (hours == 12) {
        ampm = ' PM';
    } else if (hours == 0) {
        hours = 12;
    } else if (hours > 12) {
        hours -= 12;
        ampm = ' PM';
    }

    return hours + ':' + minutes + ' ' + ampm;
}

// search list used in admin > manage users

function myFunction() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
/**
 * Function to show message in Popup
 * Parameters:
 * Title,Body*/
function showModalMessage(modal_title_text, modal_body_text) {

    commonModalTitle = $(".common_modal_title");
    commonModalTitle.text(modal_title_text);

    commonModalBody = $(".common_modal_body");
    commonModalBody.text(modal_body_text);

    $("#common_modal").modal('show');

}

function makeSlotsActive(){

    $('.bg-green').click(function () {

        $('.bg-green').removeClass('selected-box');
        $(this).addClass('selected-box');
        $('#booking-btn').removeClass('disabled');

        var schedule_id = $(this).attr('rel');
        $('#schedule-id').val(schedule_id);

    });
}



function disableAllSlots(){
    var docAwayWholeDay = false;
    if ($('#schedule-checkbox').is(':checked')){
        docAwayWholeDay = true;
        blockAllSlots();
    } else {
        docAwayWholeDay = false;
        var schedule_date = $("#schedule_date").val(); // booking_date.format('yyyy-mm-dd');
        var schedule_doctor_id = $("#schedule_doctor_id").val();
        var slotsArea = $("#schedule_slots_area");
        var slotsSelectable ="Multi";
        generateSlots(schedule_date, schedule_doctor_id, slotsArea, slotsSelectable);
    }
    return docAwayWholeDay;
}

function generateSlots (selectedDate, selectedDoctorID, slotsArea, slotsSelectable){
    console.log(selectedDate);
    console.log(selectedDoctorID);
    console.log(slotsArea);

    $.ajax({

        url:"initFunctions.php",
        method:"POST",
        data: {bookingDate:selectedDate, doctorID: selectedDoctorID, fnToCall: "getSlots"},
        success: function(resp){
            //console.log(resp);
            resp = JSON.parse(resp);
            //console.log(resp);
            var htmlGrid = '<table id="schedule_slots_table" class="table table-bordered"> <tr>';

            /*for(var i=0;i<res.length;i++){}*/
            var i=0;
            for(x in resp){

                if(i%6==0){   // remainder division
                    htmlGrid += "</tr><tr>";
                }

                var record = resp[x];
                //console.log(record.id);
                var strTime = convertTime(record.start_time); // convert time to 12 hour format
                var strColour = decideBgColour(record.status);
                // htmlGrid += "<td rel='"+record.schedule_id+"' class='slot-box bg-"+strColour+"'>"+strTime+"</td>"; // generating
                htmlGrid += "<td rel='"+record.schedule_id+"' class='slot-box bg-"+strColour+"'>" +
                    strTime +
                    "<input type='hidden' name='slot_ids[]'   value='"+record.schedule_id+"' />" +
                    "<input type='hidden' name='slot_status[]' value='"+record.status+"'  />"
                "</td>";
                i++;
            }
            htmlGrid += "</tr> </table>";
            slotsArea.html(htmlGrid);
            //console.log(htmlGrid);
            if (slotsSelectable == "Multi"){
                makeMultiSlotToggle();
            } else {
                makeSlotsActive();
            }

        }
    });
}

function makeMultiSlotToggle(){

    $('#schedule-btn').removeClass('disabled');
    $('#schedule-checkbox').removeClass('disabled');

    $('.slot-box').click(function(){

       var slotIDElement = $(this).find('[name="slot_ids[]"]');
       var slotStatus = $(this).find('[name="slot_status[]"]');

       if($(slotStatus).val()=='available'){
           $(this).removeClass('bg-green');
           $(this).addClass('bg-grey');
           $(slotStatus).val('unavailable');

       }else{
           $(this).removeClass('bg-grey');
           $(this).addClass('bg-green');
           $(slotStatus).val('available');
       }

    });


    console.log();
}


function blockAllSlots(){

    var element = $('.slot-box');

    if($(element).hasClass('bg-green')){
        $(element).removeClass('bg-green');
        $(element).addClass('bg-grey');
    }
}



function generateSlotsMultiTable (selectedDate, selectedDoctorID, slotsArea, slotsSelectable){
    //console.log(selectedDate);
    //console.log(selectedDoctorID);
    //console.log(slotsArea);

    var sentDate = new Date(selectedDate);
    var sentDateYmd = formatDateYmd(sentDate);
    var sentDateShow = formatDateYmdDmy(sentDateYmd);
    //console.log(sentDate);
    //console.log(sentDateYmd);
    //console.log(sentDateShow);

    var htmlGrid = '<br />';
    htmlGrid += '<table id="choice_slots_area_new" class="table table-bordered"> <tr>';
    for (i=0;i<=6;i++) { // for seven grids - for seven days

        htmlGrid +=  '<td><p>' + sentDateShow + '</p>';
        $.ajax({
            url: "initFunctions.php",
            method: "POST",
            async: false,
            data: {bookingDate: sentDateYmd, doctorID: selectedDoctorID, fnToCall: "getSlots"},
            success: function(resp) {
                htmlGrid += '<table id="choice_slots_table_new" class="table table-bordered"> <tr> ';
                //console.log(resp);
                resp = JSON.parse(resp);
                //console.log(resp);

                var j = 0;
                for (x in resp) {

                    if (j % 6 == 0) {   // remainder division
                        htmlGrid += '</tr><tr>';
                    }
                    //console.log(x);
                    var record = resp[x];
                    //console.log(record.status);
                    var strTime = convertTime(record.start_time); // convert time to 12 hour format
                    //console.log(strTime);
                    var strColour = decideBgColour(record.status);
                    htmlGrid += "<td rel='" + record.schedule_id + "' class='slot-box bg-" + strColour + "'>" +
                        strTime +
                        "<input type='hidden' name='slot_ids[]'   value='" + record.schedule_id + "' />" +
                        "<input type='hidden' name='slot_status[]' value='" + record.status + "'  />" +
                        "</td>";

                    j++;
                }
                htmlGrid += '</tr></table>';
                console.log(htmlGrid);
            }
        });
        //console.log(htmlGridReturned);
        htmlGrid += '</td>';
        sentDateYmd = incrementDate(sentDateYmd,1);
        //console.log(sentDateYmd);
        sentDateYmd = formatDateYmd(sentDateYmd);
        //console.log(sentDateYmd);
        sentDateShow = formatDateYmdDmy(sentDateYmd);
        //console.log(sentDateShow);
        //console.log(htmlGrid);
    }
    htmlGrid += '</tr></table>';
    //console.log(htmlGrid);
    slotsArea.html(htmlGrid);
    if (slotsSelectable == "Multi"){
        makeMultiSlotToggle();
    } else {
        makeSlotActiveMultiTable();
    }
}

// manageUserChoice.php
function makeSlotActiveMultiTable(){

    $('.bg-green').click(function () {

        $('.bg-green').removeClass('selected-box');
        $(this).addClass('selected-box');
        $('#update-booking-btn').removeClass('disabled');

        var schedule_id = $(this).attr('rel');
        $('#new-schedule-id').val(schedule_id);

    });
}

function incrementDate(dateInput,increment) {
    var dateFormatTotime = new Date(dateInput);
    var increasedDate = new Date(dateFormatTotime.getTime() +(increment *86400000));
    return increasedDate;
}

function formatDateYmd(d){
    var month = d.getMonth()+1;
    var day = d.getDate();

    var thisDate = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;
    return thisDate;
}

function formatDateYmdDmy(d) {
    var reversedDate =  d.split("-").reverse().join("/");
    //console.log("hi = " + reversedDate);
    return reversedDate;
}

function check(e,box){
    var self = $(this);

    if ((self.length == 1) && (e.keyCode >= 48 && e.keyCode <= 57) ) {
        switch (parseInt(box)) {
            case 1:
                $('#medicare2a').focus();
                break;
            case 2:
                $('#medicare3a').focus();
                break;
            case 3:
                $('#medicare4a').focus();
                break;
            case 4:
                $('#medicare1b').focus();
                break;
            case 5:
                $('#medicare2b').focus();
                break;
            case 6:
                $('#medicare3b').focus();
                break;
            case 7:
                $('#medicare4b').focus();
                break;
            case 8:
                $('#medicare5b').focus();
                break;
            case 9:
                $('#medicare1c').focus();
                break;
            case 10:
                $('#medicare1d').focus();
/*            default:
                $('#password').focus();*/
        }
    }
}


