<?php
ob_start();
session_start();
include_once 'config.php';

/* General Functions*/
function connectDB(){
    global $conn;
    $conn = mysqli_connect(HOST, DBUSER, DBPASS, DBNAME);
    return $conn;
}

function getRecord($query){
    $numRecords = 0;
    $conn = connectDB(); // connect to db, using connectDB function
    $result = mysqli_query($conn, $query);
    if ($result){
        $numRecords = mysqli_num_rows($result);
    }
    $arrNumRecords = array(
        'num'       => $numRecords,
        'records'   => $result
    );
    return $arrNumRecords;
}

function getSingleRecord($table, $where)
{
    $query = "SELECT * FROM " . $table . " WHERE " . $where;
    $arrNumRecords = getRecord($query);
    if ($arrNumRecords["num"] == 1) {
        $row = mysqli_fetch_assoc($arrNumRecords["records"]);
        return $row;
    }
    return false;
}

function addRecord($table, $arrColsVals)
{

    /*Table Columns*/
    $arrCols = array_keys($arrColsVals); // picking the table column names sent by user
    $strCols = implode(',', $arrCols); // making string of columns separated by comma
   /*TAble Columns*/

    /*record values */
    $strVals = '"'.implode('","', $arrColsVals).'"';
    /*record values */

    $conn = connectDB();
    $query = 'insert into  '.$table. '('.$strCols.') values ('.$strVals.')';
    $isInserted  = mysqli_query($conn, $query);

    return $isInserted;
}

function updateRecord($table, $where, $arrColsVals)
{

    $sqlColsVals = array();

    foreach($arrColsVals as $col=>$colVal){
        $tmpColVal = "$col='$colVal'";
        array_push($sqlColsVals, $tmpColVal);
    }

    $query = 'UPDATE '.$table.'  SET ';
    $query .= implode(', ', $sqlColsVals);
    $query .= ' where '.$where;

    // update users set column='value' where CONDITION

    $conn = connectDB();
    $isUpdated  = mysqli_query($conn, $query);
    return $isUpdated;
}

function deleteRecord($table, $where){
    $query = 'DELETE FROM '.$table.' WHERE '.$where;
    $conn = connectDB();
    $isDeleted = mysqli_query($conn, $query);
    return $isDeleted;
}

function convertTo24hrFormat($time12hrFormat){ // 07:15pm -> 19:15:00
    $time24hrFormat  = DATE("H:i", STRTOTIME($time12hrFormat));
    return $time24hrFormat;
}

function convertTo12hrFormat($time24hrFormat){ // 19:15:00 -> 07:15 pm
    $time12hrFormat  = DATE("g:i a", STRTOTIME($time24hrFormat));
    return $time12hrFormat;
}

function getSession(){
    $userID = $_SESSION['userID'];
    echo $userID;
}
/* General Functions Ends */

/* Users Functions */
function doLogin($username, $password){
    $row="";

    if(!empty($username) && !empty($password)){
        $conn = connectDB();

        $username = mysqli_real_escape_string($conn, $username);
        $password = mysqli_real_escape_string($conn, $password);

        $query = "select * from users where medicare='{$username}'";
        $arrNumRecords = getRecord($query);

        if($arrNumRecords['num']>0){
            $records = $arrNumRecords['records'];
            $row = mysqli_fetch_assoc($records);
            if ($password == dec_enc('decrypt',$row['password'])) { //password_verify($password, $row['password'])
                $_SESSION['isLogged'] = true;
                $_SESSION['userID'] = $row['id'];
            }
        }
    }
    return $row;
}

function loginRecover($username, $email){
    $row="";

    if(!empty($username) && !empty($email)){
        $conn = connectDB();

        $username = mysqli_real_escape_string($conn, $username);
        $password = mysqli_real_escape_string($conn, $email);

        $query = "select * from users where medicare='{$username}'";
        $arrNumRecords = getRecord($query);

        if($arrNumRecords['num']>0){
            $records = $arrNumRecords['records'];
            $row = mysqli_fetch_assoc($records);
            if ($email == $row['email']) {
                $_SESSION['isUser'] = true;
                $_SESSION['userID'] = $row['id'];
            }
        }
    }
    return $row;
}

function doSignup($medicare, $firstName, $lastName, $email, $address, $suburb, $postCode, $state, $phone, $password){

    $validSignUp = false;

    if (!empty($medicare) && !empty($firstName) && !empty($lastName) && !empty($email) && !empty($password) && !empty($phone)) {
        $conn = connectDB(); // connect to db, using connectDB func$first_name = mysqli_real_escape_string($conn, $first_name);
        $medicare = mysqli_real_escape_string($conn, $medicare);
        $firstName = mysqli_real_escape_string($conn, $firstName);
        $lastName = mysqli_real_escape_string($conn, $lastName);
        $email = mysqli_real_escape_string($conn, $email);
        $address = mysqli_real_escape_string($conn, $address);
        $suburb = mysqli_real_escape_string($conn, $suburb);
        $postCode = mysqli_real_escape_string($conn, $postCode);
        $state = mysqli_real_escape_string($conn, $state);
        $phone = mysqli_real_escape_string($conn, $phone);
        $password = mysqli_real_escape_string($conn, $password);

        $query = "select * from users where medicare='{$medicare}'";
        $arrNumRecords = getRecord($query);

        if(!$arrNumRecords['num'] > 0){
            // hashing the password
            $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
            $hashedPwd = dec_enc('encrypt', $password);
            // Insert the user into the database
            $query = "INSERT INTO users 
                      (medicare, 
                      first_name, 
                      last_name, 
                      email, 
                      address, 
                      suburb, 
                      post_code, 
                      state, 
                      phone, 
                      password) 
                      VALUES 
                      ('$medicare', 
                      '$firstName', 
                      '$lastName', 
                      '$email', 
                      '$address', 
                      '$suburb', 
                      '$postCode', 
                      '$state', 
                      '$phone', 
                      '$hashedPwd');";
            mysqli_query($conn, $query);
            $validSignUp = true;
        }
    }
    return $validSignUp;
}

function printUserRecordTable() {
    $output = '
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
    <table class="table table-bordered" id="myTable">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th width="30%">Operations</th>
                    </tr>';

                    $query = "select * from users ORDER BY id DESC";
                    $arrNumRecords = getRecord($query);
                    $records = array();
                    if($arrNumRecords['num']>0){
                        $records = $arrNumRecords['records'];
                    }

                    while($row = mysqli_fetch_array($records))
                    {

                        $output .=  '<tr>
                                        <td>
                                            '. $row["first_name"] .'
                                        </td>
                                        <td>
                                            '. $row["email"] .'
                                        </td>
                                        <td>
                                            <input type="button" name="view" value="view" id="'. $row["id"] .'" class="btn btn-info btn-xs view_user_data" />
                                            <input type="button" name="edit" value="edit" id="'. $row["id"] .'" class="btn btn-info btn-xs edit_user_data" />
                                            <input type="button" name="delete" value="delete" id="'. $row["id"] .'" class="btn btn-info btn-xs delete_user_data" />
                                        </td>
                                    </tr>';

                    }

    $output .= '</table>';
    return $output;
}

function getUserAppointmentsByDateDoctor($date, $id){

    $where = " ds.doctor_id = '$id' and ds.schedule_date = '$date'";
    return getUserAppointment($where);
}

function getUserAppointmentById($id){

    $where = " ua.id = '$id'";
    return getUserAppointment($where);
}

function getUserAppointment($where=""){

    $query = "SELECT *, ua.id as appointment_id from user_appointments ua 
                      INNER JOIN users u on ua.user_id = u.id
                      INNER JOIN doctor_schedules ds on ua.schedule_id=ds.id
                      INNER JOIN doctors d on d.id = ds.doctor_id
                      INNER JOIN time_slots ts on ds.slot_id = ts.id ";

    if(!empty($where)){
        $query .= " where $where";
        $arrNumRecords = getRecord($query);
        return $arrNumRecords;
    }
}

// table for manage-user-appointments page
function printAppointmentsTable() {

    $output = '
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for ID">
    <table class="table table-bordered" id="myTable">
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Time</th>
                        <th>User ID</th>
                        <th width="30%">Operations</th>
                    </tr>';

    if (isset($_POST["appointments_date"])) { // check of one is enough

        $date = $_POST["appointments_date"];
        $date = str_replace('/', '-', $date);
        $date = date('Y-m-d',strtotime($date));
        $id = $_POST["appointments_doctor_id"];

        $records = getUserAppointmentsByDateDoctor($date, $id);
        if($records['num']>0){
            $records = $records['records'];
            while($row = mysqli_fetch_array($records))
            {
                $output .=  '<tr>
                                    <td>'. $row["first_name"] .'</td>
                                    <td>'. $row["last_name"] .'</td>
                                    <td>'. $row["email"] .'</td>
                                    <td>'. $row["phone"] .'</td>
                                    <td>'. $row["start_time"] .'</td>
                                    <td>'. $row["user_id"] .'</td>
                                    <td>
                                        <a href="manageUserChoice.php?id='.$row["appointment_id"].'" class="btn btn-info btn-xs edit_appt_data">Edit</a>
                                        <!--<input type="button" name="edit" value="edit" id="'. $row["schedule_id"] .'" class="btn btn-info btn-xs edit_appt_data" /> -->
                                        <input type="button" name="delete" value="delete" id="'. $row["schedule_id"] .'" class="btn btn-info btn-xs delete_appt_data" />
                                    </td>
                             </tr>';
            }
        }
    }

    $output .= '</table>';
    //return $output;
    echo json_encode($output);
}

function getUserByID()
{
    if (isset($_POST["user_id"])) {
        $where = "id = '" . $_POST["user_id"] . "'";
        $record = getSingleRecord('users', $where);
        echo json_encode($record);
    }
}

function getUserByEmail($email)
{
    if (!empty($email)) {
        $where = "email = '" . $email . "'";
        $record = getSingleRecord('users', $where);
        return $record;
    }
}

function getSlotByStartTime($Start_time)
{
    if (!empty($Start_time)) {
        $where = "start_time = '" . $Start_time . "'";
        $record = getSingleRecord('time_slots', $where);
        return $record;
    }
}

// start coding for selectABC.php
function selectUser()
{
    if (isset($_POST["user_id"])) {
        $output = '';
        $query = "SELECT * FROM users WHERE id = '" . $_POST["user_id"] . "'";
        $arrNumRecords = getRecord($query);

        if ($arrNumRecords['num'] > 0) {
            $records = $arrNumRecords['records'];
        }

        $output .= '  
          <div class="table-responsive">  
               <table class="table table-bordered">';
        while ($row = mysqli_fetch_array($records)) {
            $output .= '  
                    <tr>  
                         <td width="30%"><label>First Name</label></td>  
                         <td width="70%">' . $row["first_name"] . '</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>Last Name</label></td>  
                         <td width="70%">' . $row["last_name"] . '</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>email</label></td>  
                         <td width="70%">' . $row["email"] . '</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>Phone</label></td>  
                         <td width="70%">' . $row["phone"] . '</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>User Type</label></td>  
                         <td width="70%">' . $row["user_type"] . '</td>  
                    </tr>  
                    ';
        }
        $output .= "</table></div>";
        echo $output;
    }
}

function selectDoctor(){
    if(isset($_POST["doctor_id"]))
    {
        $output = '';
        $query = "SELECT * FROM doctors WHERE id = '".$_POST["doctor_id"]."'";
        $arrNumRecords = getRecord($query);

        if($arrNumRecords['num']>0){
            $records = $arrNumRecords['records'];
        }

        $output .= '  
          <div class="table-responsive">  
               <table class="table table-bordered">';
        while($row = mysqli_fetch_array($records))
        {
            $output .= '  
                    <tr>  
                         <td width="30%"><label>Name</label></td>  
                         <td width="70%">'.$row["name"].'</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>Qualifications</label></td>  
                         <td width="70%">'.$row["qualification"].'</td>  
                    </tr>  
                    ';
        }
        $output .= "</table></div>";
        echo $output;
    }
}

function selectSlot(){
    if(isset($_POST["slot_id"]))
    {
        $output = '';
        $query = "SELECT * FROM time_slots WHERE id = '".$_POST["slot_id"]."'";
        $arrNumRecords = getRecord($query);

        if($arrNumRecords['num']>0){
            $records = $arrNumRecords['records'];
        }

        $output .= '  
          <div class="table-responsive">  
               <table class="table table-bordered">';
        while($row = mysqli_fetch_array($records))
        {
            $output .= '  
                    <tr>  
                         <td width="30%"><label>Start Time</label></td>  
                         <td width="70%">'.$row["start_time"].'</td>  
                    </tr>  
                    <tr>  
                         <td width="30%"><label>End Time</label></td>  
                         <td width="70%">'.$row["end_time"].'</td>  
                    </tr>  
                    ';
        }
        $output .= "</table></div>";
        echo $output;
    }
}
// end coding for selectABC.php

// Start booking.php book button
function insertBookingRecord($ajaxCall=true, $schedule_id=-1){

    $response = array(
        'success' => false,
        'data' => "server error"
    );

    $userID = getLoggedUserId();
    $isInserted = false;
    $myFlag = true;

    if($userID){

        if($ajaxCall){
         $scheduleID = $_POST["schedule-id"];
        }
        else{
            $scheduleID = $schedule_id;
        }

        $arrColsVals = array(
            'schedule_id' => $scheduleID,
            'user_id' => $userID,
        );

        // check for double booking
        $row = getSingleRecord('doctor_schedules', 'id='.$scheduleID);
        $dateWanted = $row['schedule_date'];

        $query = "SELECT * FROM user_appointments WHERE user_id = '".$userID."'";
        $arrNumRecords = getRecord($query);

        if ($arrNumRecords['num'] >0){
            $records = $arrNumRecords['records'];
            while ($row = mysqli_fetch_array($records)) {
                $scheduleID = $row['schedule_id'];
                $record = getSingleRecord('doctor_schedules', 'id='.$scheduleID);
                if ($dateWanted == $record['schedule_date']) {
                   $myFlag = false;
                }
            }
        }
        if ($myFlag) {
            $isInserted = addRecord('user_appointments', $arrColsVals);
            $response["success"] = $isInserted;
        }

        if ($isInserted) {
            $response["data"] = "Record was Inserted.";

            $arrNewBookingVals = array(
                "status" => "booked"
            );

            $where = " id = $scheduleID ";
            updateRecord('doctor_schedules', $where, $arrNewBookingVals);

        } else {
            $response["data"] = "Record was not Inserted.";
        }
    }
    else{

        $response['data']  = "Your Session timed out, Please login and try again";
    }

    if($ajaxCall)
        echo json_encode($response); // printing json string instead of normal string output;
    else
        return $response;
}
// end booking.php book button

// START - manage-user-appointment, btn DELETE to delete an appointment
function deleteAppointment($ajaxCall=true, $schedule_id = -1){

    $response = array(
        'success' => false,
        'data' => "server error"
    );

    $userId = getLoggedUserId(); // this will be useful to record the details of person who deleted the appointment

    if($userId){

        if($ajaxCall){
            $scheduleID = $_POST["user_appt_sch_id"];
        }else{
            $scheduleID = $schedule_id;
        }

        $where = " schedule_id = $scheduleID ";
        $isDeleted = deleteRecord('user_appointments', $where);
        $response["success"] = $isDeleted;
        if ($isDeleted) {
            $response["data"] = "Record was Deleted.";

            $arrNewDoctorScheduleVals = array(
                "status" => "available"
            );

            $where = " id = $scheduleID ";
            updateRecord('doctor_schedules', $where, $arrNewDoctorScheduleVals);

        } else {
            $response["data"] = "Record was not deleted.";
        }
    }
    else{

        $response['data']  = "Your Session timed out, Please login and try again";
    }

    if($ajaxCall)
        echo json_encode($response); // printing json string instead of normal string output;
    else
        return $response;
}

// END - manage-user-appointment, btn DELETE to delete an appointment





function updateAppointment(){

    $response = array(
        'success' => false,
        'data' => "server error"
    );

    $oldScheduleID = $_POST["oScheduleId"];
    $newScheduleID = $_POST["nScheduleId"];

    $isDeleted = deleteAppointment(false, $oldScheduleID);



    if($isDeleted["success"]==true){

        $response["data"] = "Old Booking Deleted, But New Booking Failed";
        $isInserted = insertBookingRecord(false, $newScheduleID);

        if($isInserted["success"]==true){
             $response["data"] = "Booking Updated Successfully";
             $response["success"] = true;
        }

    }

    echo json_encode($response);
}


// start manageDoctorSchedules.php save button
function updateDoctorSchedules(){

    $response = array(
        'success' => false,
        'data' => "server error"
    );
    $flag = "OFF";
    $arrSlotIDs = $_POST["slot_ids"]; // array
    $arrSlotStatus = $_POST["slot_status"]; // array
    $docAwayWholeDay = filter_var($_POST["docAwayWholeDay"], FILTER_VALIDATE_BOOLEAN);

    for ($i = 0; $i < count($arrSlotIDs); $i++) {
        if ($flag == "OFF") {
            if ($docAwayWholeDay){
                $newScheduleVals = array(
                    "status" => "unavailable"
                );
                $tempSlotID = $arrSlotIDs[$i];
                $where = " id = $tempSlotID ";
                $isUpdated = updateRecord('doctor_schedules', $where, $newScheduleVals);
                if ($isUpdated and $flag == "OFF") {
                    $response = array(
                        'success' => true,
                        'data' => "Doctor schedules were updated successfully"
                    );
                } else {
                    $flag = "ON";
                    $response = array(
                        'success' => false,
                        'data' => "Doctor schedules were not updated successfully"
                    );
                }
            } else {
                $tempSlotStatus = $arrSlotStatus[$i];
                $newScheduleVals = array(
                    "status" => $tempSlotStatus
                );
                $tempSlotID = $arrSlotIDs[$i];
                $where = " id = $tempSlotID ";
                $isUpdated = updateRecord('doctor_schedules', $where, $newScheduleVals);
                if ($isUpdated and $flag == "OFF") {
                    $response = array(
                        'success' => true,
                        'data' => "Doctor schedules were updated successfully"
                    );
                } else {
                    $flag = "ON";
                    $response = array(
                        'success' => false,
                        'data' => "Doctor schedules were not updated successfully"
                    );
                }
            }
        }
    }

    echo json_encode($response); // printing json string instead of normal string output;
}
// end manageDoctorSchedules.php save button

function getLoggedUserId(){

    if(isset($_SESSION['userID']))
        return $_SESSION['userID'];
    else
        return false;
}

function addUser(){
    if(!empty($_POST)){

        $record = getUserByEmail($_POST['add_user_email']);
        // initialise an array - set default value. This displays if nothing else works
        $response = array(
                    "success"=>false,
                    "data" => "Server Error"
        );

        if($record==false){ // if(!$user) => !false => true => !true = false
            $conn = connectDB(); // connect to db, using connectDB function
            // security
            $firstName = mysqli_real_escape_string($conn, $_POST['add_user_fname']);
            $lastName = mysqli_real_escape_string($conn, $_POST['add_user_lname']);
            $email = mysqli_real_escape_string($conn, $_POST['add_user_email']);
            $password = mysqli_real_escape_string($conn, $_POST['add_user_pass']);
            $phone = mysqli_real_escape_string($conn, $_POST['add_user_phone']);

            // hashed password
            $password = password_hash($password, PASSWORD_DEFAULT);

            if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) { // only character allowed in Fname
                $response["data"] = "Only letters and white space allowed in first name.";
            } else {
                if (!preg_match("/^[a-zA-Z ]*$/", $lastName)) { // only character allowed in Lname
                    $response["data"] = "Only letters and white space allowed in last name.";
                } else {
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // check the formatting of email
                        $response["data"] = "Invalid email format.";
                    } else {
                            if (!preg_match("/^(0|\+61|0061)(2|4|3|7|8)[0-9]{8}$/", $phone)) { // ausee land and mobile
                            $response["data"] = "Only Australian Land Lines and mobiles allowed.";
                        } else {

                            $columnValuesArray = array(
                                'first_name' => $firstName,
                                'last_name' => $lastName,
                                'password' => $password,
                                'email' => $email,
                                'phone' => $phone,
                            );

                            $isInserted = addRecord('users', $columnValuesArray);
                            /*$isInserted = false; // Mimic  always false*/
                            $response["success"] = $isInserted; // true (inserted) or false (not inserted )
                            if ($isInserted) {
                                $response["data"] = printUserRecordTable(); // echo here send data to super where this
                            } else {
                                $response["data"] = "Record was not Inserted.";
                            }
                        }
                    }
                }
            }
        }
        else{

            $response["success"] = false;
            $response["data"]  = "Email Already Registered";
        }

        echo json_encode($response); // {"success":true, "data": "html or msg"};
    }
}

function updateUser()
{
    if(!empty($_POST)){

        $columnValuesArray = array(
            'email' => $_POST['edit_user_email'],
            'phone' => $_POST['edit_user_phone'],
        );

        $where = "id = '". $_POST['edit_user_id']."'";

        $isUpdated = updateRecord('users', $where ,  $columnValuesArray);
        echo printUserRecordTable(); // echo here send data to super where this
    }
}

function deleteUser()
{
    if(!empty($_POST)){
        $where = "id='".$_POST['userid']."'";
        $isDeleted = deleteRecord('users', $where);
        echo printUserRecordTable();
    }
}

// delete old Data - doctor_schedules and user_appointments
function deleteOldData()
{
    $today = new DateTime();
    $date =  $today->format('Y-m-d');
    // delete data of previous doctor_schedules
    $where = "schedule_date < '".$date."'";
    $isDeleted = deleteRecord('doctor_schedules', $where);
    // get id of first row of doctor_schedules
    $where = "doctor_id = '1' and slot_id = '1' and schedule_date = '".$date."'";
    $row =  getSingleRecord('doctor_schedules', $where);
    // delete data of previous user_appointments
    $where = "schedule_id < '".$row["id"]."'";
    $isDeleted = deleteRecord('user_appointments', $where);
}


// manage doctors
function printDoctorRecordTable() {
    $output = '
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
    <table class="table table-bordered" id="myTable">
                    <tr>
                        <th>Name</th>
                        <th>Qualification</th>
                        <th width="30%">Operations</th>
                    </tr>';

    $query = "select * from doctors ORDER BY id DESC";
    $arrNumRecords = getRecord($query);
    $records = array();
    if($arrNumRecords['num']>0){
        $records = $arrNumRecords['records'];
    }

    while($row = mysqli_fetch_array($records))
    {

        $output .=  '<tr>
                                        <td>
                                            '. $row["name"] .'
                                        </td>
                                        <td>
                                            '. $row["qualification"] .'
                                        </td>
                                        <td>
                                            <input type="button" name="view" value="view" id="'. $row["id"] .'" class="btn btn-info btn-xs view_doctor_data" />
                                            <input type="button" name="edit" value="edit" id="'. $row["id"] .'" class="btn btn-info btn-xs edit_doctor_data" />
                                            <input type="button" name="delete" value="delete" id="'. $row["id"] .'" class="btn btn-info btn-xs delete_doctor_data" />
                                        </td>
                                    </tr>';
    }
    $output .= '</table>';
    return $output;
}

function getDoctor()
{
    if (isset($_POST["doctor_id"])) {
        $where = "id = '" . $_POST["doctor_id"] . "'";
        $record = getSingleRecord('doctors', $where);
        echo json_encode($record);
        //structure is like {first_name:"Jovial", email:"jovial@yahoo.com}
    }
    else if(isset($_POST["bookingDate"])){
        $arrDoctors = [];
        if($_POST["joinQuery"]!="Yes"){

            $bookingDate = $_POST["bookingDate"];

            $query = "select doctor_id from doctor_schedules where schedule_date='$bookingDate' GROUP by doctor_id";
            $arrNumRecords = getRecord($query);

            if($arrNumRecords["num"]>0){
                while($row = mysqli_fetch_assoc($arrNumRecords["records"])){
                    $doctor_id = $row['doctor_id'];

                    $where = " id='$doctor_id' ";
                    $row = getSingleRecord('doctors',$where);
                    array_push($arrDoctors, $row);
                }
            }
        }
        else{
            $bookingDate = $_POST["bookingDate"];

            /*$sqlQuery = "select ds.doctor_id, d.* from doctor_schedules ds, doctors d
                         where ds.schedule_date='$booking_date' and d.id=ds.doctor_id                        
                         GROUP by ds.doctor_id";*/

            $query = "select ds.doctor_id, d.* from doctor_schedules ds 
                         INNER JOIN  doctors d ON d.id=ds.doctor_id 
                         where ds.schedule_date='$bookingDate'
                         GROUP BY ds.doctor_id
                         ";

            $arrNumRecords = getRecord($query);
            if($arrNumRecords["num"]>0){
                while($row = mysqli_fetch_assoc($arrNumRecords["records"])){
                    array_push($arrDoctors, $row);
                }
            }
        }
        echo json_encode($arrDoctors);
    }
}

//
function getAppointment() {

    $userID = getLoggedUserId(); // this will be useful to record the details of person who updated the appointment

    if($userID){
        $scheduleID = $_POST["user_appt_sch_id"];
    }

    $query = "SELECT ua.*, u.*, ds.*, ts.*, d.*
            FROM ((((user_appointments ua 
            INNER JOIN users u ON ua.user_id = u.id)
            INNER JOIN doctor_schedules ds ON ua.schedule_id = ds.id)
            INNER JOIN time_slots ts ON ds.slot_id = ts.id)
            INNER JOIN doctors d ON ds.doctor_id = d.id)
            WHERE ds.id = '$scheduleID'
            ";
    $arrNumRecords = getRecord($query);
    $records = array();
    $row = "";
    if($arrNumRecords['num']>0) {
        $records = $arrNumRecords['records'];
        $row = mysqli_fetch_array($records); //$row["user_id"]
    }

    $_SESSION['rowSession'] = $row;
    echo json_encode($row);
    return $row;
}

function getAllDoctors(){

    $arrDoctors = [];
    $query = "select * from doctors";
    $arrNumRecords = getRecord($query);
    if($arrNumRecords['num']>0){
        while($row = mysqli_fetch_assoc($arrNumRecords['records'])){
            array_push($arrDoctors, $row);

        }
    }

    return $arrDoctors;
}

function getDoctorScheduleCount($date, $doctor){

    /*echo "<pre>";
    print_r($date);

    print_r($doctor);
    echo "</pre>";*/

    $date = $date->format("Y-m-d");

    $query = "select * from doctor_schedules where schedule_date='$date' and doctor_id='$doctor[id]'";
    $arrNumRecords = getRecord($query);
    return $arrNumRecords["num"];
}

function addDoctor(){
    if(!empty($_POST)){
        $arrColsVals = array(
            'name' => $_POST['add_doctor_name'],
            'qualification' => $_POST['add_doctor_qualification'],
        );

        $isInserted = addRecord('doctors', $arrColsVals);

        echo printDoctorRecordTable(); // echo here send data to super where this
    }
}

function updateDoctor()
{

    if(!empty($_POST)){

        $arrColsVals = array(
            'name' => $_POST['edit_doctor_name'],
            'qualification' => $_POST['edit_doctor_qualification'],
        );

        $where = "id = '". $_POST['edit_doctor_id']."'";

        $isUpdated = updateRecord('doctors', $where ,  $arrColsVals);
        echo printDoctorRecordTable(); // echo here send data to super where this
    }
}

function deleteDoctor()
{
    if(!empty($_POST)){
        $where = "id='".$_POST['doctor_id']."'";
        $isDeleted = deleteRecord('doctors', $where);
        echo printDoctorRecordTable();
    }
}

function printTimeSlotsTable() {
    $output = '
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
    <table class="table table-bordered" id="myTable">
                    <tr>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th width="30%">Operations</th>
                    </tr>';

    $query = "select * from time_slots ORDER BY start_time";
    $arrNumRecords = getRecord($query);
    $records = array();
    if($arrNumRecords['num']>0){
        $records = $arrNumRecords['records'];
    }

    while($row = mysqli_fetch_array($records))
    {

        $output .=  '<tr>
                                        <td>
                                            '. $row["start_time"] .'
                                        </td>
                                        <td>
                                            '. $row["end_time"] .'
                                        </td>
                                        <td>
                                            <input type="button" name="view" value="view" id="'. $row["id"] .'" class="btn btn-info btn-xs view_slot_data" />
                                            <input type="button" name="edit" value="edit" id="'. $row["id"] .'" class="btn btn-info btn-xs edit_slot_data" />
                                            <input type="button" name="delete" value="delete" id="'. $row["id"] .'" class="btn btn-info btn-xs delete_slot_data" />
                                        </td>
                                    </tr>';
    }
    $output .= '</table>';
    return $output;
}

function getSlot()
{
    if (isset($_POST["slot_id"])) {
        $where = "id = '" . $_POST["slot_id"] . "'";
        $row = getSingleRecord('time_slots', $where);
        echo json_encode($row);
//        {first_name:"Daler", email:"daler@yahoo.com}
    }
}

function addSlot(){
    if(!empty($_POST)){
        $startTime  = convertTo24hrFormat($_POST['add_slot_start_time']);
        $endTime  = convertTo24hrFormat($_POST['add_slot_end_time']);

        // initialise an array - set default value. This displays if nothing else works
        $response = array(
            "success"=>false,
            "data" => "Server Error"
        );

        // initialised
        $slot = getSlotByStartTime("10:00:00");

        //get minutes from start_time
        $timeMinutes = date('i', strtotime($startTime)); //

        //compare with  00, 15, 30, 45
        if ($timeMinutes%15 == 0){
            $slot = getSlotByStartTime($startTime);
            if($slot==false) {
                $conn = connectDB(); // connect to db, using connectDB function
                $startTime = mysqli_real_escape_string($conn, $startTime); // security
                $endTime = mysqli_real_escape_string($conn, $endTime); // security

                if (!preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $startTime)){
                    $response["data"] = "Enter time in proper format. e.g. hh:mm am";
                } else {
                    $arrColsVals = array(
                        'start_time' => $startTime,
                        'end_time' => $endTime,
                    );

                    $isInserted = addRecord('time_slots', $arrColsVals);
                    $response["success"] = $isInserted;
                    if ($isInserted) {
                        $response["data"] = printTimeSlotsTable(); // echo here send data to super where this
                    } else {
                        $response["data"] = "record was not inserted.";
                    }
                }
            }
            else {
                $response["success"] = false;
                $response["data"] = "Slot already exists";
            }
        } else {
            $response["success"] = false;
            $response["data"] = "Choose time from drop-down menu options";
        }

        echo json_encode($response);
    }
}

function updateSlot()
{
    if(!empty($_POST)){
        $arrColsVals = array(
            'start_time' => $_POST['edit_slot_start_time'],
            'end_time' => $_POST['edit_slot_end_time'],
        );

        $where = "id = '". $_POST['edit_slot_id']."'";

        $isUpdated = updateRecord('time_slots', $where ,  $arrColsVals);
        echo printTimeSlotsTable(); // echo here send data to super where this
    }
}

function deleteSlot()
{
    if(!empty($_POST)){
        $where = "id='".$_POST['slot_id']."'";
        $isDeleted = deleteRecord('time_slots', $where);
        echo printTimeSlotsTable();
    }
}


// table in booking menu
function getSlots()
{
    if(isset($_POST["bookingDate"], $_POST["doctorID"])){
        $arrSlots = [];
        $doctorID = $_POST["doctorID"];
        $bookingDate = str_replace('/', '-', $_POST["bookingDate"]);
        $bookingDate = date("Y-m-d", strtotime("$bookingDate"));

        $query = "SELECT ts.id as slot_id, ds.id as schedule_id,  ts.start_time, ds.status 
                      FROM doctor_schedules ds INNER JOIN time_slots ts  ON ts.id = ds.slot_id 
                      WHERE ds.schedule_date = '$bookingDate' AND ds.doctor_id = '$doctorID'
                      order by slot_id
                     ";

        $arrNumRecords = getRecord($query);
        if($arrNumRecords["num"]>0){
            while($row = mysqli_fetch_assoc($arrNumRecords["records"])){
                array_push($arrSlots, $row);
            }
        }
        echo json_encode($arrSlots);
    }
}


function getAllSlots(){

    $query = "select * from time_slots";
    return getRecord($query);

}

function getScheduleID(){
    if(isset($_POST["slot_id"]))
    {
        $date = str_replace('/', '-', $_POST["booking_date"]);
        $scheduleDate = date("Y-m-d", strtotime($date) );
        $where = "slot_id = '"
            . $_POST["slot_id"] .
            "' AND doctor_id = '"
            .$_POST["doctor_id"].
            "' AND schedule_date = '"
            .$scheduleDate.
            "'";
        $row = getSingleRecord('doctor_schedules', $where);
        echo json_encode($row);

        /*
        $sql = "SELECT id FROM doctor_schedules WHERE slot_id = '".$_POST["slot_id"]."' AND doctor_id = '".$_POST["doctor_id"]."' AND schedule_date = '".$schedule_date."' ";
        $arrNumRecords = getRecord($sql);

        if($arrNumRecords['num']>0){
            $result = $arrNumRecords['records'];
        }
        echo $result;
        */
    }
}

function generateDoctorSchedule()
{
    $today = new DateTime();
//    echo $today->format('Y-m-d');
    for($i=0;$i<14;$i++){

        $date = clone $today; // cloning object , make it a copy, assignment operator is not used
        $date->modify($i." day");

        $arrDoctors = getAllDoctors();

        foreach($arrDoctors as $doctor){

            $doctorScheduleCount = getDoctorScheduleCount($date, $doctor);
            //echo $doctorSchedule . "<br/>";
            //echo $date->format("Y-m-d") . "<br/>";
            //echo $doctor["id"] . "<br/>";
            //echo  "<br/>";
            if($doctorScheduleCount==0){
                $arrNumRecords = getAllSlots();
                if($arrNumRecords["num"]>0){
                    foreach($arrNumRecords["records"] as $slot){
                        createDoctorSchedule($date, $doctor, $slot);
                    }
                }
            }
        }
    }
}



function createDoctorSchedule($date, $doctor, $slot){
        //echo $date->format('Y-m-d') . "<br/>";
        //echo $doctor["id"] . "<br/>";

        $table = "doctor_schedules";
        $arrColsVals = array(
            'doctor_id' => $doctor["id"],
            'slot_id' => $slot["id"],
            'status' => 'available',
            'schedule_date' => $date->format("Y-m-d")
        );

        addRecord($table, $arrColsVals);

}

function decideBgColour($cellStatus) {
    $strBgColour = 'aqua';
    if ($cellStatus === 'available'){
        $strBgColour = 'green';
    } else if ($cellStatus === 'booked'){
        $strBgColour = 'red';
    }else if ($cellStatus === 'unavailable'){
        $strBgColour = 'grey';
    }
    return $strBgColour;
}

function convertTime ($isoTime) {
    $hours   = intval(substr($isoTime, 0, 2));
        $minutes = substr($isoTime,3, 2);
        $ampm    = ' AM';

    if ($hours == 12) {
        $ampm = ' PM';
    } else if ($hours == 0) {
        $hours = 12;
    } else if ($hours > 12) {
        $hours -= 12;
        $ampm = ' PM';
    }

    return $hours . ':' . $minutes . ' ' . $ampm;
}

function dec_enc($action, $string) {
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is jovial secret key';
    $secret_iv = 'This is jovial secret iv';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
}
?>