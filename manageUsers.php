<?php
include_once 'header.php';
?>

<div class="container">
    <h3 align="center">Manage users' records.</h3>
    <br />
    <div class="table-responsive">
        <div align="right">
            <button type="button" name="add_user" id="add_user" data-toggle="modal" data-target="#add_user_data_modal" class="btn btn-warning">Add user</button>
        </div>
        <br />
        <div id="user_table">
            <?php
                echo printUserRecordTable();
            ?>
        </div>
    </div>
</div>

<div id="view_user_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User's Details</h4>
            </div>
            <div class="modal-body" id="user_detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="add_user_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a new user.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="add_user_form">
                    <label>Enter First Name</label>
                    <input type="text" name="add_user_fname" id="add_user_fname" class="form-control" />
                    <input type="hidden" name="fnToCall" value="addUser">
                    <br />
                    <label>Enter Last Name</label>
                    <input type="text" name="add_user_lname" id="add_user_lname" class="form-control">
                    <br />
                    <label>Enter Password</label>
                    <input type="text" name="add_user_pass" id="add_user_pass" class="form-control">
                    <br />
                    <label>Enter email</label>
                    <input type="email" name="add_user_email" id="add_user_email" class="form-control" />
                    <br />
                    <label>Enter Phone</label>
                    <input type="text" name="add_user_phone" id="add_user_phone" class="form-control" />
                    <br />
                    <input type="submit" name="insert_user" id="insert_user" value="Insert User" class="btn btn-success" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="edit_user_data_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit details of user.</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="edit_user_form">
                    <label>ID</label>
                    <input type="text" name="edit_user_id" id="edit_user_id" class="form-control" value = "" readonly>
                    <br />
                    <label>First Name</label>
                    <input type="text" name="edit_user_fname" id="edit_user_fname" class="form-control" value = "" readonly>
                    <br />
                    <label>Last Name</label>
                    <input type="text" name="edit_user_lname" id="edit_user_lname" class="form-control" value = "" readonly>
                    <br />
                    <label>Email</label>
                    <input type="email" name="edit_user_email" id="edit_user_email" class="form-control" value = "" >
                    <br />
                    <label>Phone</label>
                    <input type="text" name="edit_user_phone" id="edit_user_phone" class="form-control" value = "" >
                    <br />
                    <label>User Type:<span id="edit_user_userType"></span></label>
                    <br />
                    <input type="submit" name="update_user" id="update_user" value="Update User" class="btn btn-success" />

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




<?php
include_once 'footer.php';
?>