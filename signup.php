<?php
include_once 'header.php';
?>

<div class="container">
    <?php if(isset($_GET['errorMsg'])): ?>
        <div class="row">
            <div class="col-md-12">
                <?php
                    echo ("Message: ");
                    echo $_GET["errorMsg"];
                ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-4 col-centered">
            <form class="form-signup" action="" method="post">

                <h2 class="form-signup-heading">SIGN UP</h2>

                <div class="form-group">
                    <label for="medicare1" class="sr-only">Medicare</label>
                    <table> <tr>
                            <td><input name="medicare1a" type="text" id="medicare1a" class="form-control2" pattern="[1-9]" autofocus=""
                                       required="" maxlength="1" data-cip-id="medicare1a" onKeyUp="check(event,'1')"></td>
                            <td><input name="medicare2a" type="text" id="medicare2a" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare2a" onKeyUp="check(event,'2')"></td>
                            <td><input name="medicare3a" type="text" id="medicare3a" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare3a" onKeyUp="check(event,'3')"></td>
                            <td><input name="medicare4a" type="text" id="medicare4a" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare4a" onKeyUp="check(event,'4')"></td><td>----</td>
                            <td><input name="medicare1b" type="text" id="medicare1b" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare1b" onKeyUp="check(event,'5')"></td>
                            <td><input name="medicare2b" type="text" id="medicare2b" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare2b" onKeyUp="check(event,'6')"></td>
                            <td><input name="medicare3b" type="text" id="medicare3b" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare3b" onKeyUp="check(event,'7')"></td>
                            <td><input name="medicare4b" type="text" id="medicare4b" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare4b" onKeyUp="check(event,'8')"></td>
                            <td><input name="medicare5b" type="text" id="medicare5b" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare5b" onKeyUp="check(event,'9')"></td><td>----</td>
                            <td><input name="medicare1c" type="text" id="medicare1c" class="form-control2" pattern="[0-9]"
                                       required="" maxlength="1" data-cip-id="medicare1c" onKeyUp="check(event,'10')"></td><td>----</td>
                            <td><input name="medicare1d" type="text" id="medicare1d" class="form-control2" pattern="[1-9]"
                                       required="" maxlength="1" data-cip-id="medicare1d"></td>
                        </tr></table>
                </div>

                <div class="form-group">
                    <label for="firstName" class="sr-only">First Name</label>
                    <input name="firstName" type="text" id="firstName" class="form-control"
                           placeholder="First name"
                           required="" data-cip-id="firstName">
                </div>

                <div class="form-group">
                    <label for="lastName" class="sr-only">Last name</label>
                    <input name="lastName" type="text" id="lastName" class="form-control"
                           placeholder="Last name"
                           required="" data-cip-id="lastName">
                </div>

                <div class="form-group">
                    <label for="email" class="sr-only">Email address</label>
                    <input name="email" type="email" id="email" class="form-control"
                           placeholder="Email Address"
                           required="" data-cip-id="email">
                </div>

                <div class="form-group">
                    <label for="address" class="sr-only">Password</label>
                    <input name="address" type="text" id="address" class="form-control"
                           placeholder="Address"
                           required="" data-cip-id="address">
                </div>

                <div class="form-group">
                    <label for="suburb" class="sr-only">Password</label>
                    <input name="suburb" type="text" id="suburb" class="form-control"
                           placeholder="Suburb"
                           required="" data-cip-id="suburb">
                </div>

                <div class="form-group">
                    <label for="postCode" class="sr-only">Password</label>
                    <input name="postCode" type="text" id="papostCodepostCodesword" class="form-control"
                           placeholder="Post code"
                           required="" data-cip-id="postCode">
                </div>

                <div class="form-group">
                    <label for="state" class="sr-only">Password</label>
                    <input name="state" type="text" id="state" class="form-control"
                           placeholder="State"
                           required="" data-cip-id="state">
                </div>

                <div class="form-group">
                    <label for="phone" class="sr-only">Password</label>
                    <input name="phone" type="text" id="phone" class="form-control"
                           placeholder="Phone Number"
                           required="" data-cip-id="phone">
                </div>

                <div class="form-group">
                    <label for="password" class="sr-only">Password</label>
                    <input name="password" type="password" id="password" class="form-control"
                           placeholder="Password"
                           required="" data-cip-id="password">
                </div>

                <div class="form-group">
                    <label for="confirmPassword" class="sr-only">Password</label>
                    <input name="confirmPassword" type="password" id="confirmPassword" class="form-control"
                           placeholder="Password again"
                           required="" data-cip-id="confirmPassword">
                </div>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
            </form>
        </div>
    </div>
</div>

<?php
    include_once 'footer.php';
?>
<?php

if(isset($_POST['medicare1a'])){
    $medicare = $_POST['medicare1a'].
            $_POST['medicare2a'].
            $_POST['medicare3a'].
            $_POST['medicare4a'].
            $_POST['medicare1a'].
            $_POST['medicare2b'].
            $_POST['medicare3b'].
            $_POST['medicare4b'].
            $_POST['medicare5b'].
            $_POST['medicare1c'].
            $_POST['medicare1d'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirmPassword'];
    if ($password == $confirmPassword) {

        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $suburb = $_POST['suburb'];
        $postCode = $_POST['postCode'];
        $state = $_POST['state'];
        $phone = $_POST['phone'];

        $isValidSignUp = doSignUp($medicare, $firstName, $lastName, $email, $address, $suburb, $postCode, $state, $phone, $password);
        $location = 'signup.php?errorMsg=Sign Up Failed';
        if ($isValidSignUp) {
            $location = './login.php';
        }
        header("location: $location");
    }
    else {
        $location = 'signup.php?errorMsg=Password confirmation does not match';
    }

}
?>
